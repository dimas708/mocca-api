<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['register'=>false]);

Route::group(['middleware' => ['auth', 'admin']], function () {
    
    Route::get('/',  'DashboardController@index');
    Route::get('/admin', 'DashboardController@index');
    Route::get('/admin/manageuser', 'ManageUserController@index');
    Route::get('/admin/managearticle', 'ManageArticleController@index');
    Route::get('/admin/managecategory', 'ManageKategoriController@index');
    Route::get('/admin/administrator', 'AdministratorController@index');

    // Admin Manage Artikel
    Route::get('/admin/detailarticle/{id}', 'ManageArticleController@detail_artikel');
    Route::get('/admin/managearticle/block/{id}', 'ManageArticleController@block_artikel');
    Route::get('/admin/managearticle/unblock/{id}', 'ManageArticleController@unblock_artikel');
    Route::get('/admin/managearticle/delete/{id}', 'ManageArticleController@hapus_artikel');

    // Admin Manage User
    Route::get('/admin/manageuser/block/{id}', 'ManageUserController@block_user');
    Route::get('/admin/manageuser/unblock/{id}', 'ManageUserController@unblock_user');
    Route::get('/admin/detailuser/{id}', 'ManageUserController@detail_user');
    Route::post('/admin/edituser/{id}', 'ManageUserController@edit_profil');

    //Admin Manage Kategori
    Route::get('/admin/addcategory', 'ManageKategoriController@add_kategori');
    Route::get('/admin/managecategory/{id}', 'ManageKategoriController@delete');
    Route::get('/admin/detailcategory/{id}', 'ManageKategoriController@detail_kategori');
    Route::post('/admin/addcategory/add','ManageKategoriController@tambah_kategori');
    Route::post('/admin/editcategory/{id}','ManageKategoriController@edit_kategori');
    Route::get('/admin/deletecategory/{id}','ManageKategoriController@delete');

    //Administrator
    Route::get('/admin/addAdmin', 'AdministratorController@add_admin');
    Route::get('/admin/detailadmin', 'AdministratorController@detail_admin');
    Route::post('/admin/addAdmin/add','AdministratorController@tambah_admin');
    Route::post('/admin/editadmin/{id}','AdministratorController@edit_admin');
    Route::post('/admin/credentials/','AdministratorController@postCredentials');
}); 


