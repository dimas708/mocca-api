<?php

use Illuminate\Http\Request;
//use Auth;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
// Route::post('/auth', function (Request $request){
   
// });
Route::post('/register','Auth\RegisterController@register');
Route::post('/login','Auth\LoginController@auth');
Route::post('/forgot', 'Auth\ForgotPasswordController@__invoke');

Route::post('/kategori/tambah','Api\KategoriController@tambah_kategori');
Route::post('/kategori/edit/{id}','Api\KategoriController@edit_kategori');
Route::delete('/kategori/{id}','Api\KategoriController@hapus_kategori');
Route::get('/kategori','Api\KategoriController@kategori');
Route::get('/kategori/{kategori}','Api\KategoriController@getartikel_bykategori');
Route::get('/kategori/detail/{id}','Api\KategoriController@detail_kategori');


Route::post('/artikel/tambah','Api\ArtikelController@tambah_artikel');
Route::get('/artikel','Api\ArtikelController@show_artikel');
Route::post('/artikel/edit/{id}','Api\ArtikelController@edit_artikel');
Route::delete('/artikel/{id}','Api\ArtikelController@hapus_artikel');
Route::get('/artikel/detail/{id}','Api\ArtikelController@detail_artikel');
Route::get('/artikel/list/{id}','Api\ArtikelController@draft_artikel');
Route::get('/artikel/popular/','Api\ArtikelController@popular_artikel');
Route::get('/artikel/search/{search}','Api\ArtikelController@search_artikel');


Route::post('/artikel/comment','Api\CommentController@create_comment');
Route::delete('/artikel/comment/{id}', 'Api\CommentController@delete');
Route::post('/artikel/comment/edit/{id}', 'Api\CommentController@edit');

Route::post('/artikel/like','Api\LikeController@like');
Route::delete('/artikel/like/{id}','Api\LikeController@hapus_like');

Route::get('/user/{id}', 'Api\UserController@index');
Route::post('/profil/edit/{id}','Api\UserController@edit_profil');