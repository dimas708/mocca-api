<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class artikel extends Model
{
  protected $table = "artikel";

  protected $primaryKey= "id";
  protected $fillable = ['title','img','kategori_id','teks','user_id', 'status'];

    public function kategori(){ 
      return $this->belongsTo('App\kategori'); 
    }
    public function user(){ 
      return $this->belongsTo('App\User'); 
    }
    public function comment(){
      return $this->hasMany('App\comment');
    }
    public function like(){
      return $this->hasMany('App\like');
    }
    public function follow(){
      return $this->hasMany('App\follow');
  }
}
