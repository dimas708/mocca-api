<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Artikel;
use App\Kategori;

class DashboardController extends Controller
{
    //
    public function index()
    {
        $artikel = Artikel::get();
        $artikel_count = $artikel->count();

        $kategori = Kategori::get();
        $kategori_count = $kategori->count();

        $user = User::get();
        $user_count = $user->where('role','user')->count();

     	return view('admin.dashboard',['artikel' => $artikel_count, 'kategori' => $kategori_count, 'user' => $user_count]);
    }
}
