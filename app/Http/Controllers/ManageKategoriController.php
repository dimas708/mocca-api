<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kategori;


class ManageKategoriController extends Controller
{
    public function index()
    {
        $kategori = Kategori::orderBy('created_at', 'DESC')->get();

     	return view('admin.manageCategory',['data' => $kategori]);
    }

    public function add_kategori()
    {
        return view('admin.addCategory');
    }

    public function tambah_kategori(Request $request){
        $file = $request->file('img');

        $nama_file = rand()."_".$file->getClientOriginalName();
    
        $direct = 'upload';
        $file->move($direct,$nama_file);
        
        $data = Kategori::create([
            'kategori'=> $request->kategori,
            'img' => $nama_file,
            'desc'=> $request->desc
        ]);
        return redirect('admin/managecategory');
    }
    
    public function edit_kategori(Request $request, $id){
        $nama_file = $request->hidden_image;
        $file = $request->file('img');
        if($file != '')
        {
            $request->validate([
                'kategori'    =>  'required',
                'img'     =>  'image|max:2048',
            ]);

            $nama_file = rand() . '.' . $file->getClientOriginalExtension();
            $file->move('upload', $nama_file);
        }
        else
        {
            $request->validate([
                'kategori'    =>  'required',
            ]);
        }

        $data = array(
            'kategori'  =>   $request->kategori,
        );

        if($file != ''){
            $data['img'] = $nama_file;
        }
        if($request->filled('desc')){
            $data['desc'] = $request->desc;
        }
  
        kategori::whereId($id)->update($data);

        return redirect()->back();
    }

    public function detail_kategori($id){
        $kategori = kategori::where('id',$id)->get();

        return view('admin.detailCategory', ['data' => $kategori]);
    }

    public function delete($id){
        $kategori = Kategori::find($id);
        $kategori->delete();

        return redirect('admin/managecategory');
    }
}
