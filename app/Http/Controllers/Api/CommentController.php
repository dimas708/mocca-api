<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Comment;
use App\Artikel;
use App\Http\Controllers\Controller;

class CommentController extends Controller
{
    public function create_comment(Request $request){   
        $data = comment::create([
            'comment'=> $request->comment,
            'artikel_id' => $request->artikel_id,
            'user_id' => $request->user_id,
        ]);
        return $data;
    }

    public function delete($id){
        $data = comment::where('id',$id)->delete();
        return 'Data berhasil dihapus';
    }
    
    public function edit(Request $request , $id){
        $data = comment::find($id);
        $data->comment = $request->comment;
        
        $data->update();

        return comment::find($id);
    }

}
