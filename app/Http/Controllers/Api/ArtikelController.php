<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Artikel;
use App\User;
use App\Kategori;
use App\Comment;
use App\Http\Controllers\Controller;

class ArtikelController extends Controller
{
    public function show_artikel(){
        $artikel = Artikel::where('status','active')->with(['user','kategori','like'])->
        orderBy('created_at', 'DESC')->paginate(8);
        // $artikel->load('user');
        return $artikel;
    }

    public function draft_artikel($id_user){
        $data = Artikel::where('user_id', $id_user)->with([ 
                'user','like','kategori'])->orderBy('created_at', 'DESC')->get();
        return $data;  
    }

    public function tambah_artikel(Request $request){   
        $file = $request->file('img');

        $nama_file = time()."_".$file->getClientOriginalName();
    
        $direct = 'upload';
        $file->move($direct,$nama_file);
        
        $data = Artikel::create([
            'title'=> $request->title,
            'img' => $nama_file,
            'kategori_id'=> $request->kategori_id,
            'teks' => $request->teks,
            'user_id' => $request->user_id,
            'status' => 'active',
        ]);
        return $data;
    }

    public function edit_artikel(Request $request, $id){
        $nama_file = $request->hidden_image;
        $file = $request->file('img');
        if($file != '')
        {
            $nama_file = rand() . '.' . $file->getClientOriginalExtension();
            $file->move('upload', $nama_file);
        }

        // $data = array(
        //     'title'            =>   $request->title,
        //     'img'              =>   $nama_file,
        //     'kategori_id'      =>   $request->kategori_id,
        //     'teks'             =>   $request->teks
        // );

        if($request->filled('title')){
            $data['title'] = $request->title;
        }
        if($file != ''){
            $data['img'] = $nama_file;
        }
        if($request->filled('kategori_id')){
            $data['kategori_id'] = $request->kategori_id;
        }
        if($request->filled('teks')){
            $data['teks'] = $request->teks;
        }
  
        Artikel::whereId($id)->update($data);

        return $data; 
    }

    public function hapus_artikel($id){
		Artikel::where('id',$id)->delete();
 
		return "Data berhasil dihapus";
    }

    public function detail_artikel($id)
    {
        $artikel = Artikel::where('id',$id)->with([
            'kategori',
            'user',
            'comment.user',
            'like.user' 
            ])->withCount('like')->get();
        return $artikel;
    }

    public function popular_artikel(){
        $artikel = Artikel::where('status','active')->with(['user', 'like', 'kategori'])->
                   withCount('like')->
                   orderBy('like_count', 'DESC')->limit('10')->get();
        return $artikel;
    }

    public function search_artikel($search){

        $artikel = Artikel::where('status','active')
                    ->where('title', 'LIKE', '%'.$search.'%')
                    ->with(['user', 'like', 'kategori'])
                    ->withCount('like')
                    ->orderBy('created_at', 'DESC')->get();

        if(count($artikel) > 0) {
            return $artikel;
        }else {
            return "Tidak ada data yang ditemukan";
        }
    }
    
}
