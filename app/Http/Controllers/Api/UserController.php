<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Artikel;
use App\User;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    public function index($id){
        $data = user::with('artikel.like')->where('id',$id)->get();
        return $data;
    }

    public function edit_profil(Request $request, $id){
        $nama_file = $request->hidden_image;
        $file = $request->file('photo');
        if($file != '')
        {
            $nama_file = rand() . '.' . $file->getClientOriginalExtension();
            $file->move('upload', $nama_file);
        }

        // $data = array(
        //     'name' => $request->name,
        // );
        if($request->filled('name')){
            $data['name'] = $request->name;
        }
        if($file != ''){
            $data['photo'] = $nama_file;
        }
        if($request->filled('bio')){
            $data['bio'] = $request->bio;
        }
  
        User::whereId($id)->update($data);

        return $data; 
    }
    
}
