<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Follow;
use App\User;
use App\Artikel;
use App\Http\Controllers\Controller;

class FollowController extends Controller
{
    public function follow(Request $request){   
        $data = Follow::create([
            'user_id' => $request->user_id,
            'target_id' => $request->target_id,
        ]);
        return $data;
    }

    public function hapus_follow($id){
		Follow::where('id',$id)->delete();
 
		return "Follow berhasil dihapus";
    }
}
