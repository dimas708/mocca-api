<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Like;
use App\Artikel;
use App\Http\Controllers\Controller;

class LikeController extends Controller
{
    public function like(Request $request){   
        $data = Like::create([
            'like'=> $request->like,
            'artikel_id' => $request->artikel_id,
            'user_id' => $request->user_id,
        ]);
        return $data;
    }

    public function hapus_like($id){
		Like::where('id',$id)->delete();
 
		return "Like berhasil dihapus";
    }
}
