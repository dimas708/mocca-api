<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Kategori;
use App\Artikel;
use App\Http\Controllers\Controller;

class KategoriController extends Controller
{
    public function kategori(){
        $kategori = kategori::all();
        return $kategori;
    }

    public function tambah_kategori(Request $request){
        $file = $request->file('img');

        $nama_file = rand()."_".$file->getClientOriginalName();
    
        $direct = 'upload';
        $file->move($direct,$nama_file);
        
        $data = Kategori::create([
            'kategori'  => $request->kategori,
            'img'       => $nama_file,
            'desc'      => $request->desc
        ]);
        return $data;
    }

    public function edit_kategori(Request $request, $id){
        $nama_file = $request->hidden_image;
        $file = $request->file('img');
        if($file != '')
        {
            $request->validate([
                'kategori'  =>  'required'
            ]);

            $nama_file = rand() . '.' . $file->getClientOriginalExtension();
            $file->move('upload', $nama_file);
        }
        else
        {
            $request->validate([
                'kategori'  =>  'required',
            ]);
        }

        $data = array(
            'kategori'  =>   $request->kategori,
        );

        if($file != ''){
            $data['img'] = $nama_file;
        }
        if($request->filled('desc')){
            $data['desc'] = $request->desc;
        }
  
        kategori::whereId($id)->update($data);

        return $data; 
    }

    public function hapus_kategori($id){
        $kategori = Kategori::find($id);
        $kategori->delete();

        return "Data berhasil dihapus";
    }

    public function getartikel_bykategori($kategori){
        $id_kat = kategori::where('kategori',$kategori)->first()->id;
        $kategori = Artikel::where('kategori_id',$id_kat)->with('user')->orderBy('created_at', 'DESC')->paginate(8);
        return $kategori;
    }
    
    public function detail_kategori($id){
        $kategori = kategori::where('id',$id)->get();
        return $kategori;
    }
    
}
