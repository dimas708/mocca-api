<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin';
    // protected function redirectTo() {
    //     if ( Auth::user()->role == 'admin' ) {

    //         return 'admin';

    //     } else {

    //         return 'home';

    //     }
    // }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function auth(Request $request){
        if($valid = Auth::attempt($request->all()))
        {   if(Auth::user()->status == 'active'){
            $user = Auth::user(); 
            $user->api_token = str_random(100);
            $user->save();

            $user->makeVisible('api_token');

            return $user;
            }
            else if(Auth::user()->status == 'blocked'){
            return response()->json(['message'=>'Maaf, Akun anda telah diblokir']);  
            } 
        }
        return response()->json([
            'message'=>'Akun belum terdaftar'
        ]);
    }
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
