<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Artikel;
use App\User;

class ManageArticleController extends Controller
{
    //
    public function index()
    {
        $artikel = Artikel::orderBy('created_at', 'DESC')->get();
        $artikel->load('user');

     	return view('admin.manageArticle',['data' => $artikel]);
 
    }

    public function detail_artikel($id){
        $data = Artikel::where('id', $id)->get();

        return view('admin.detailArticle', ['data' => $data]);
    }

    public function block_artikel($id)
    {
        $artikel = Artikel::find($id);
        $artikel->status = 'blocked';
        $artikel->save();

        return redirect()->back();
    }

    public function unblock_artikel($id)
    {
        $artikel = Artikel::find($id);
        $artikel->status = 'active';
        $artikel->save();

        return redirect()->back();
    }

    public function hapus_artikel($id){
        $artikel = Artikel::find($id);
        $artikel->delete();
 
		return redirect('admin/managearticle');
    }
}
