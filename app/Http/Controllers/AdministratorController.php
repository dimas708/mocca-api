<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Support\Facades\Validator;

class AdministratorController extends Controller
{
    //
    public function index()
    {
    	$data = User::where('role','admin')->get();
 
    	return view('admin.administrator',['data' => $data]);
 
    }
    public function add_admin()
    {
        return view('admin.addAdmin');
    }

    public function tambah_admin(Request $request){
        $message = [
            'unique'=>'Akun sudah terdaftar',
            'required' => 'Lengkapi data'
        ];
        $this->validate($request, [
           'name'=>'required',
           'email'=>'required|unique:users,email',
           'password'=>'required|min:8|confirmed',          
        ],$message);
        $data = $request->all();
        $data['api_token'] = str_random(100);
        $data['password'] = bcrypt($data['password']);
        $data['photo'] = 'default.jpg';
        $data['status'] = 'active';
        $data['role'] = 'admin';

        $user = User::create($data);

        if($user){
            return redirect('admin/administrator');
        }
       
    }
    public function edit_admin(Request $request, $id){
        $nama_file = $request->hidden_image;
        $file = $request->file('photo');
        if($file != '')
        {
            $nama_file = rand() . '.' . $file->getClientOriginalExtension();
            $file->move('upload', $nama_file);
        }
        
        if($request->filled('name')){
            $data['name'] = $request->name;
        }
        if($file != ''){
            $data['photo'] = $nama_file;
        }
        if($request->filled('bio')){
            $data['bio'] = $request->bio;
        }

        User::whereId($id)->update($data);

        return redirect()->back();
    }

    public function admin_credential_rules(array $data){
        $messages = [
            'current-password.required' => 'Please enter current password',
            'password.required' => 'Please enter password',
        ];

        $validator = Validator::make($data, [
            'current-password' => 'required',
            'password' => 'required|same:password',
            'password_confirmation' => 'required|same:password',     
        ], $messages);

        return $validator;
    }  

    public function postCredentials(Request $request){
        if(Auth::Check())
        {
            $request_data = $request->All();
            $validator = $this->admin_credential_rules($request_data);
            if($validator->fails())
            {
            return response()->json(array('error' => $validator->getMessageBag()->toArray()), 400);
            }
            else
            {  
            $current_password = Auth::User()->password;           
            if(Hash::check($request_data['current-password'], $current_password))
            {           
                $user_id = Auth::User()->id;                       
                $obj_user = User::find($user_id);
                $obj_user->password = Hash::make($request_data['password']);;
                $obj_user->save(); 
                return redirect()->back();
            }
            else
            {           
                $error = array('current-password' => 'Please enter correct current password');
                return response()->json(array('error' => $error), 400);   
            }
            }        
        }
        else
        {
            return redirect()->to('/');
        }    
}

    public function detail_admin(){
        $id_user = Auth::user()->id;

        $data = User::where('id', $id_user)->get();

        return view('admin.detailadmin', ['data' => $data]);
    }
    
    public function block_admin($id)
    {
        $data = User::find($id);
        $data->status = 'blocked';
        $data->save();

        return redirect()->back();
    }

    public function unblock_admin($id)
    {
        $data = User::find($id);
        $data->status = 'active';
        $data->save();

        return redirect()->back();
    } 

}
