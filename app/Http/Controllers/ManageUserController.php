<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Http\Controllers\Controller;

class ManageUserController extends Controller
{
    //
    public function index()
    {
    	$data = User::where('role','user')->get();
 
    	return view('admin.manageUser',['data' => $data]);
 
    }
    public function detail_user($id){
        $data = User::where('id', $id)->get();

        return view('admin.detailUser', ['data' => $data]);
    }
    
    public function block_user($id)
    {
        $data = User::find($id);
        $data->status = 'blocked';
        $data->save();

        return redirect()->back();
    }

    public function unblock_user($id)
    {
        $data = User::find($id);
        $data->status = 'active';
        $data->save();

        return redirect()->back();
    } 

    public function edit_profil(Request $request, $id){
        $nama_file = $request->hidden_image;
        $file = $request->file('photo');
        if($file != '')
        {
            $nama_file = rand() . '.' . $file->getClientOriginalExtension();
            $file->move('upload', $nama_file);
        }

        // $data = array(
        //     'name'             =>   $request->name,
        //     'photo'            =>   $nama_file,
        //     'bio'              =>   $request->bio,
        // );
        
        if($request->filled('name')){
            $data['name'] = $request->name;
        }
        if($file != ''){
            $data['photo'] = $nama_file;
        }
        if($request->filled('bio')){
            $data['bio'] = $request->bio;
        }

        User::whereId($id)->update($data);

        return redirect()->back();
    }
}
