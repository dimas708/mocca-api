<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class like extends Model
{
    protected $table = 'like';

    protected $primaryKey = 'id';
    protected $fillable = ['like','artikel_id','user_id'];
    

    public function artikel(){ 
        return $this->belongsTo('App\artikel'); 
    }
    public function user(){ 
        return $this->belongsTo('App\User'); 
    }
}
