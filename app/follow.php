<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class follow extends Model
{
    protected $table = 'follow';

    protected $primaryKey = 'id';
    protected $fillable = ['user_id', 'target_id'];
    
    public function user(){ 
        return $this->belongsTo('App\User'); 
    }
    public function artikel(){ 
        return $this->belongsTo('App\artikel'); 
      }
}
