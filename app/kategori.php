<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use artikel;

class kategori extends Model
{
    protected $table = 'kategori';
    protected $fillable = ['kategori','img','desc'];

    public function artikel(){
    	return $this->hasMany('App\artikel');
    }

}
