<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class comment extends Model
{
    protected $table = 'comment';

    protected $primaryKey = 'id';
    protected $fillable = ['comment', 'artikel_id','user_id'];
    

    public function artikel(){ 
        return $this->belongsTo('App\artikel'); 
    }
    public function user(){ 
        return $this->belongsTo('App\user'); 
    }
}
