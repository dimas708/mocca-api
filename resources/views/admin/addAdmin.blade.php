@extends('layouts.master')

@section('title')
Mocca | Admin Panel
@endsection

@section('page')
Add Admin
@endsection

@section('activeTabs')
5
@endsection


@section('content')
<div class="row">
    <div class="col-md-12">
        <div style="padding: 20px;" class="card">
            <h4 class="mb-4 mt-0">Add Admin</h4>
            <form enctype="multipart/form-data" method="POST" action="/admin/addAdmin/add">
                @csrf
                <div class="form-group">
                    <label>Name</label>
                    <input type="text" name="name" class="form-control" placeholder="Name">
                </div>

                <div class="form-group">
                    <label>Email</label>
                    <input type="email" name="email" class="form-control" placeholder="Email">
                </div>

                <div class="form-group">
                    <label>Password</label>
                    <input type="password" name="password" class="form-control" placeholder="Password">
                </div>
                <div class="form-group">
                    <label>Confirm Password</label>
                    <input type="password" name="password_confirmation" class="form-control" placeholder="Confrim Password">
                </div>

                <button type="submit" class="btn btn-primary">Add</button>
            </form>
        </div>
    </div>
</div>
@endsection


@section('scripts')
    
@endsection

