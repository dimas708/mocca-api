@extends('layouts.master')

@section('title')
Mocca | Admin Panel
@endsection

@section('page')
Detail Article
@endsection

@section('activeTabs')
3
@endsection


@section('content')
<div style="padding: 20px;" class="card">
    <div class="text-center">

        <img style="width: 100%; height: 250px; object-fit: cover; margin-bottom: 20px" src="{{ url('/upload/'.$data[0]->img) }}" alt="...">
        <h5 class="title">{{$data[0]->title}}</h5>
        <p style="font-size: 12pt">
            by {{$data[0]->user->name}}
        </p>
        <div class="d-flex justify-content-center">
            <p style="margin-right: 15px">Like: {{$data[0]->like->count()}}</p>
            <p>Comment: {{$data[0]->comment->count()}}</p>
        </div>

        <div class="d-flex justify-content-center">
            @if( $data[0]->status == 'active')
                <div>
                    <a class="btn btn-warning" href="/admin/managearticle/block/{{ $data[0]->id }}">Block</a>
                </div>  
            @else
                <div>
                    <a class="btn btn-info" href="/admin/managearticle/unblock/{{ $data[0]->id }}">Activate</a>
                </div> 
            @endif
            <div style="margin-left: 15px">
                <a onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger btn-block" href="/admin/managearticle/delete/{{ $data[0]->id }}">Delete</a>
            </div>
        </div>
    
    </div>
</div>
@endsection


@section('scripts')
<script src="../../assets/js/core/jquery.min.js"></script>
<script src="../../assets/js/core/popper.min.js"></script>
<script src="../../assets/js/core/bootstrap.min.js"></script>
<script src="../../assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
<script src="../../assets/js/plugins/chartjs.min.js"></script>
<script src="../../assets/js/plugins/bootstrap-notify.js"></script>
<script src="../../assets/js/now-ui-dashboard.min.js?v=1.5.0" type="text/javascript"></script>
<script src="../../assets/demo/demo.js"></script>
@endsection

