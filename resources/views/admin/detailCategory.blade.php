@extends('layouts.master')

@section('title')
Mocca | Admin Panel
@endsection

@section('page')
Detail Category
@endsection

@section('activeTabs')
4
@endsection


@section('content')
<div class="row">
    <div class="col-md-4">
        <div style="padding: 20px;" class="card">
           <div class="text-center">

                <img style="width: 100%; height: 250px; object-fit: contain; margin-bottom: 40px" src="{{ url('/upload/'.$data[0]->img) }}" alt="...">
                <h5 class="title">{{$data[0]->kategori}}</h5>
                <p style="font-size: 16pt">
                    {{$data[0]->desc}}
                </p>
            </div>
            <div>
                <a onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger btn-block" href="/admin/deletecategory/{{ $data[0]->id }}">Delete</a>
            </div>
        </div>
    </div>

    <div class="col-md-8">
      <div style="padding: 20px;" class="card">
            <h4 class="mb-4 mt-0">Edit Category</h4>
            <form enctype="multipart/form-data" method="POST" action="/admin/editcategory/{{$data[0]->id}}">
                @csrf
                <div class="form-group">
                    <label>Category</label>
                    <input value="{{$data[0]->kategori}}" type="text" name="kategori" class="form-control" placeholder="Category">
                </div>

                <div class="form-group">
                    <label>Description</label>
                    <input value="{{$data[0]->desc}}" type="text" name="desc" class="form-control" placeholder="Description">
                </div>

                <div class="form-group">
                    <label>Image</label>
                    <input type="file" name="img" accept="image/gif|image/jpeg|image/png" class="form-control">
                </div>

                <button type="submit" class="btn btn-primary">Edit</button>
            </form>
        </div>
    </div>
</div>
@endsection


@section('scripts')
<script src="../../assets/js/core/jquery.min.js"></script>
<script src="../../assets/js/core/popper.min.js"></script>
<script src="../../assets/js/core/bootstrap.min.js"></script>
<script src="../../assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
<script src="../../assets/js/plugins/chartjs.min.js"></script>
<script src="../../assets/js/plugins/bootstrap-notify.js"></script>
<script src="../../assets/js/now-ui-dashboard.min.js?v=1.5.0" type="text/javascript"></script>
<script src="../../assets/demo/demo.js"></script>
@endsection

