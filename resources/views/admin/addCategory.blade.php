@extends('layouts.master')

@section('title')
Mocca | Admin Panel
@endsection

@section('page')
Add Category
@endsection

@section('activeTabs')
4
@endsection


@section('content')
<div class="row">
    <div class="col-md-12">
        <div style="padding: 20px;" class="card">
            <h4 class="mb-4 mt-0">Add Category</h4>
            <form enctype="multipart/form-data" method="POST" action="/admin/addcategory/add">
                @csrf
                <div class="form-group">
                    <label>Category Name</label>
                    <input type="text" name="kategori" class="form-control" placeholder="Category">
                </div>

                <div class="form-group">
                    <label>Description</label>
                    <input type="text" name="desc" class="form-control" placeholder="Description">
                </div>

                <div class="form-group">
                    <label>Image</label>
                    <input type="file" name="img" accept="image/gif|image/jpeg|image/png" class="form-control">
                </div>

                <button type="submit" class="btn btn-primary">Add</button>
            </form>
        </div>
    </div>
</div>
@endsection


@section('scripts')
    
@endsection

