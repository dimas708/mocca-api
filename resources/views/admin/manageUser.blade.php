@extends('layouts.master')

@section('title')
Mocca | Admin Panel
@endsection

@section('page')
Manage User
@endsection

@section('activeTabs')
2
@endsection


@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">

            <div class="card-header">
                <h4 class="card-title">User List</h4>
            </div>

            <div class="card-body">

                <div class="table-responsive">
                    <table class="table">
                    <thead class=" text-primary">
                        <th>ID</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Created</th>
                        <th></th>
                        <th class="text-center">Action</th>
                    </thead>
                    <tbody>
                        @foreach($data as $d)
                            <tr>
                                <td>{{ $d->id }}</td>
                                <td>{{ $d->name }}</td>
                                <td>{{ $d->email}}</td>
                                <td>{{ $d->created_at}}</td>
                                <td></td>
                                <td class="text-center">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-6 px-1">
                                                <a class="btn btn-success btn-block" href="/admin/detailuser/{{ $d->id }}">Detail</a>
                                            </div>
                                            @if( $d->status == 'active')
                                                <div class="col-6 px-1">
                                                    <a class="btn btn-warning btn-block" href="/admin/manageuser/block/{{ $d->id }}">Suspend</a>
                                                </div>  
                                            @else
                                                <div class="col-6 px-1">
                                                    <a class="btn btn-info btn-block" href="/admin/manageuser/unblock/{{ $d->id }}">Activate</a>
                                                </div> 
                                            @endif
                                
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        
                    </tbody>
                    </table>
                </div>

            </div>

        </div>
    </div>
</div>
@endsection


@section('scripts')
    
@endsection

