@extends('layouts.master')

@section('title')
Mocca | Admin Panel
@endsection


@section('page')
Manage Category
@endsection


@section('activeTabs')
4
@endsection


@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">

            <div class="card-header d-flex align-items-center justify-content-between">
                <h4 class="m-0">Category</h4>
                <h6 class="m-0"><a style="text-decoration: none;" href="/admin/addcategory">add category</a></h6>
            </div>

            <div class="card-body">

                <div class="table-responsive">
                    <table class="table">
                    <thead class=" text-primary">
                        <th>Image</th>
                        <th>Category</th>
                        <th>Created</th>
                        <th class="text-center">Action</th>
                    </thead>
                    <tbody>
                        @foreach($data as $d)
                            <tr>
                                <td><img width="50px" src="{{ url('/upload/'.$d->img) }}"></td>
                                <td>{{ $d->kategori}}</td>
                                <td>{{ $d->created_at}}</td>
                                <td class="text-center">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-6 px-1">
                                                <a class="btn btn-success btn-block" href="/admin/detailcategory/{{ $d->id }}">Detail</a>
                                            </div>
                                            <div class="col-6 px-1">
                                                <a onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger btn-block" href="/admin/deletecategory/{{ $d->id }}">Delete</a>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        
                    </tbody>
                    </table>
                </div>

            </div>

        </div>
    </div>
</div>
@endsection


@section('scripts')
@endsection

