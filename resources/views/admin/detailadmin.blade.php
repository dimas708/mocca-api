@extends('layouts.master')

@section('title')
Mocca | Admin Panel
@endsection

@section('page')
PROFILE
@endsection

@section('activeTabs')
6
@endsection


@section('content')
<div class="row">
    <div class="col-md-8">
      <div style="padding: 20px;" class="card">
            <h4 class="mb-4 mt-0">Edit Admin</h4>
            <form enctype="multipart/form-data" method="POST" action="/admin/editadmin/{{$data[0]->id}}">
                @csrf
                <div class="form-group">
                    <label>Name</label>
                    <input value="{{$data[0]->name}}" type="text" name="name" class="form-control" placeholder="Name">
                </div>
                <div class="form-group">
                    <label>Bio</label>
                    <input value="{{$data[0]->bio}}" type="text" name="bio" class="form-control" placeholder="Bio">
                </div>
                <div class="form-group">
                    <label>Email</label>
                    <input value="{{$data[0]->email}}"  name="email" class="form-control" placeholder="Email" disabled="">
                </div>

                <div class="form-group">
                    <label>Profile Photo</label>
                    <input type="file" name="photo" accept="image/gif|image/jpeg|image/png" class="form-control">
                </div>
                <button type="submit" class="btn btn-primary">Edit</button>
            </form>
        </div>

        <div style="padding: 20px;" class="card">
            <h4 class="mb-4 mt-0">Change Password</h4>
            <form id="form-change-password" role="form" method="POST" action="{{ url('/admin/credentials') }}" novalidate class="form-horizontal">
                <div class="form-group">             
                    <label>Current Password</label>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}"> 
                    <input type="password" class="form-control" id="current-password" name="current-password" placeholder="Password">
                </div>
                <div class="form-group">
                    <label>New Password</label>
                    <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                </div>
                <div class="form-group">
                    <label>Confirm Password</label>
                    <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" placeholder="Confirm Password">
                </div>
                <button type="submit" class="btn btn-primary">SUBMIT</button>
              </form>
        </div>

    </div>
    

    <div class="col-md-4">
        <div class="card card-user">
        @foreach ( $data as $d )
            <div class="card-body">
            <div class="text-center">
                <a href="">
                    <img class="avatar border-gray" src="{{ url('/upload/'.$d->photo) }}" alt="...">
                    <h5 class="title">{{$d->name}}</h5>
                </a>
                <p class="description">
                    {{$d->email}}
                </p>
            </div>
            <p class="description text-center">
                {{$d->bio}}
            </p>
            </div>
            
            {{-- <div class="container">
                @if( $d->status == 'active')
                    <a class="btn btn-warning btn-block" href="/admin/manageuser/block/{{ $d->id }}">Suspend</a>
                @else  
                    <a class="btn btn-info btn-block" href="/admin/manageuser/unblock/{{ $d->id }}">Activate</a>
                @endif
            </div> --}}
        @endforeach
        </div>
    </div>
</div>
@endsection


@section('scripts')
@endsection

