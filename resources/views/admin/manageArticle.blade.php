@extends('layouts.master')

@section('title')
Mocca | Admin Panel
@endsection


@section('page')
Manage Article
@endsection


@section('activeTabs')
3
@endsection


@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">

            <div class="card-header">
                <h4 class="card-title">Article Data</h4>
            </div>

            <div class="card-body">

                <div class="table-responsive">
                    <table class="table">
                    <thead class=" text-primary">
                        <th>Title</th>
                        <th>Created</th>
                        <th>Author</th>
                        <th>Status</th>
                        <th class="text-center">Action</th>
                    </thead>
                    <tbody>
                        @foreach($data as $d)
                            <tr>
                                <td>{{ $d->title }}</td>
                                <td>{{ $d->created_at}}</td>
                                <td>{{ $d->user->name}}</td>
                                <td>{{ $d->status}}</td>
                                <td class="text-center">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-4 px-1">
                                                <a class="btn btn-success btn-block" href="/admin/detailarticle/{{ $d->id }}">Detail</a>
                                            </div>  

                                            @if( $d->status == 'active')
                                                <div class="col-4 px-1">
                                                    <a class="btn btn-warning btn-block" href="/admin/managearticle/block/{{ $d->id }}">Block</a>
                                                </div>  
                                            @else
                                                <div class="col-4 px-1">
                                                    <a class="btn btn-info btn-block" href="/admin/managearticle/unblock/{{ $d->id }}">Activate</a>
                                                </div> 
                                            @endif
                                            
                                            <div class="col-4 px-1">
                                                <a onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger btn-block" href="/admin/managearticle/delete/{{ $d->id }}">Delete</a>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        
                    </tbody>
                    </table>
                </div>

            </div>

        </div>
    </div>
</div>
@endsection


@section('scripts')
@endsection

