@extends('layouts.master')

@section('title')
Mocca | Admin Panel
@endsection


@section('page')
Dashboard
@endsection


@section('activeTabs')
1
@endsection


@section('content')
<div class="row">
    <div class="col-md-4">
        <div class="card p-3">

            <h3 class="card-title">Total Users</h3>

            <div class="card-body">
                <h5 class="text-center" style="font-weight: bold; color: #F89406">{{ $user }} Users</h5>
            </div>
        </div>
    </div>

      <div class="col-md-4">
        <div class="card p-3">

           <h3 class="card-title">Total Articles</h3>

            <div class="card-body">
                <h5 class="text-center" style="font-weight: bold; color: #F89406">{{ $artikel }} Articles</h5>
            </div>
        </div>
    </div>

      <div class="col-md-4">
        <div class="card p-3">

            <h3 class="card-title">Total Categories</h3>

           <div class="card-body">
                <h5 class="text-center" style="font-weight: bold; color: #F89406">{{ $kategori }} Categories</h5>
            </div>
        </div>
    </div>
</div>
@endsection


@section('scripts')
    
@endsection

