@extends('layouts.master')

@section('title')
Mocca | Admin Panel
@endsection

@section('page')
Manage User
@endsection

@section('activeTabs')
2
@endsection


@section('content')
<div class="row">
    <div class="col-md-8">
      <div style="padding: 20px;" class="card">
            <h4 class="mb-4 mt-0">Edit User</h4>
            <form enctype="multipart/form-data" method="POST" action="/admin/edituser/{{$data[0]->id}}">
                @csrf
                <div class="form-group">
                    <label>Name</label>
                </div>

                <div class="form-group">
                    <label>Bio</label>
                    <input value="{{$data[0]->bio}}" type="text" name="bio" class="form-control" placeholder="Bio">
                </div>
                <div class="form-group">
                    <label>Email</label>
                    <input value="{{$data[0]->email}}"  name="email" class="form-control" placeholder="Email" disabled="">
                </div>

                <div class="form-group">
                    <label>Profile Photo</label>
                    <input type="file" name="photo" accept="image/gif|image/jpeg|image/png" class="form-control">
                </div>

                <button type="submit" class="btn btn-primary">Edit</button>
            </form>
        </div>
    </div>

    <div class="col-md-4">
        <div class="card card-user">
        @foreach ( $data as $d )
            <div class="card-body">
            <div class="text-center">
                <a href="">
                    <img class="avatar border-gray" src="{{ url('/upload/'.$d->photo) }}" alt="...">
                    <h5 class="title">{{$d->name}}</h5>
                </a>
                <p class="description">
                    {{$d->email}}
                </p>
            </div>
            <p class="description text-center">
                {{$d->bio}}
            </p>
            </div>
            <hr>


            
            <div class="container">
                @if( $d->status == 'active')
                    <a class="btn btn-warning btn-block" href="/admin/manageuser/block/{{ $d->id }}">Suspend</a>
                @else  
                    <a class="btn btn-info btn-block" href="/admin/manageuser/unblock/{{ $d->id }}">Activate</a>
                @endif
            </div>
        @endforeach
        </div>
    </div>
</div>
@endsection


@section('scripts')
<script src="../../assets/js/core/jquery.min.js"></script>
<script src="../../assets/js/core/popper.min.js"></script>
<script src="../../assets/js/core/bootstrap.min.js"></script>
<script src="../../assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
<script src="../../assets/js/plugins/chartjs.min.js"></script>
<script src="../../assets/js/plugins/bootstrap-notify.js"></script>
<script src="../../assets/js/now-ui-dashboard.min.js?v=1.5.0" type="text/javascript"></script>
<script src="../../assets/demo/demo.js"></script>
@endsection

