<ul class="nav">
    <li class="{{ $active == '1' ? 'active' : '' }}">
      <a href="/admin">
        <i class="now-ui-icons design_app"></i>
        <p>Dashboard</p>
      </a>
    </li>
    <li class="{{ $active == '2' ? 'active' : '' }}">
      <a href="/admin/manageuser">
        <i class="now-ui-icons users_single-02"></i>
        <p>Manage Users</p>
      </a>
    </li>
    <li class="{{ $active == '3' ? 'active' : '' }}">
      <a href="/admin/managearticle">
        <i class="now-ui-icons files_paper"></i>
        <p>Manage Articles</p>
      </a>
    </li>
    <li class="{{ $active == '4' ? 'active' : '' }}">
      <a href="/admin/managecategory">
        <i class="now-ui-icons design_bullet-list-67"></i>
        <p>Manage Categories</p>
      </a>
    </li>
    <li class="{{ $active == '5' ? 'active' : '' }}">
      <a href="/admin/administrator">
        <i class="now-ui-icons files_paper"></i>
        <p>Administrator</p>
      </a>
    </li>
    <li class="{{ $active == '6' ? 'active' : '' }}">
      <a href="/admin/detailadmin">
        <i class="now-ui-icons users_single-02"></i>
        <p>Profile</p>
      </a>
    </li>
</ul>