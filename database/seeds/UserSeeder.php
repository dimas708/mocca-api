<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
        	'name' => 'Admin Mocca',
        	'email' => 'mocca@gmail.com',
            'password' => bcrypt('default123'),
            'api_token'=> str_random(100),
            // 'password_confirmation' => 'default123',
            'photo' => 'default.jpg',
            'status' => 'active',
            'role' => 'admin',
            
        ]);
    }
}
